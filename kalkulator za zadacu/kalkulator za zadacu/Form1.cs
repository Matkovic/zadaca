﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace kalkulator_za_zadacu
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        string operation = "";

        private void sqrt_Click(object sender, EventArgs e)
        {
            funk("sqrt");
        }
        private void ctg_Click(object sender, EventArgs e)
        {
            funk("ctg");
        }
        private void tg_Click(object sender, EventArgs e)
        {
            funk("tg");
        }
        private void cos_Click(object sender, EventArgs e)
        {
            funk("cos");
        }
        private void sin_Click(object sender, EventArgs e)
        {
            funk("sin");
        }
        private void div_Click(object sender, EventArgs e)
        {
            funk("div");
        }
        private void puta_Click(object sender, EventArgs e)
        {
            funk("puta");
        }
        private void minus_Click(object sender, EventArgs e)
        {
            funk("minus");
        }
        private void plus_Click(object sender, EventArgs e)
        {
            funk("plus");
        }

        void funk(string operacija)
        {
            operation = operacija;
            textBox1.Enabled = true;
            if (operation != "sin" && operation != "cos" && operation != "tg" && operation != "ctg" && operation != "sqrt") textBox2.Enabled = true;
            disable_btns();
            textBox1.Text = "";
            textBox2.Text = "";
            label1.Text = "";
        }
        void disable_btns()
        {
            foreach (var item in Controls.OfType<Button>())
            {
                if (item.Tag == null) item.Enabled = false;
                else item.Enabled = true;
            }
        }
        void enable_btns()
        {
            textBox1.Enabled = false;
            textBox2.Enabled = false;
            foreach (var item in Controls.OfType<Button>())
            {
                if (item.Tag == null) item.Enabled = true;
                else item.Enabled = false;
            }



        }


        private void jednako_Click(object sender, EventArgs e)
        {
            double a;
            if (!double.TryParse(textBox1.Text, out a))
            {
                MessageBox.Show("This is a number only field");
                return;
            }
            double b = 0;
            if (operation == "plus" || operation == "minus" || operation == "puta" || operation == "div")
            {
                if (!double.TryParse(textBox2.Text, out b))
                {
                    MessageBox.Show("This is a number only field");
                    return;
                }
            }
            if (operation == "plus")
            {
                label1.Text = (a + b).ToString();
            }
            else if (operation == "minus")
            {
                label1.Text = (a - b).ToString();
            }
            else if (operation == "puta")
            {
                label1.Text = (a * b).ToString();
            }
            else if (operation == "div")
            {
                label1.Text = (a / b).ToString();
            }
            else if (operation == "sin")
            {
                label1.Text = (Math.Sin(a)).ToString();
            }
            else if (operation == "cos")
            {
                label1.Text = (Math.Cos(a)).ToString();
            }
            else if (operation == "tg")
            {
                label1.Text = (Math.Tan(a)).ToString();
            }
            else if (operation == "ctg")
            {
                label1.Text = (1 / Math.Tan(a)).ToString();
            }
            else if (operation == "sqrt")
            {
                label1.Text = (Math.Sqrt(a)).ToString();
            }
            enable_btns();
        }
    }
}
