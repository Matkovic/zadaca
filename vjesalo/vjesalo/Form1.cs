﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace vjesalo
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        Random numgen = new Random();
        string rijec = "";

        List<string> rijeci = new List<string>();
        List<string> pokusaji = new List<string>();


        private void Form1_Load(object sender, EventArgs e)
        {
            using (System.IO.StreamReader reader = new System.IO.StreamReader("C:/Users/danij/Desktop/rijeci.txt"))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    rijeci.Add(line);
                }

            }
            rijec = (rijeci.ElementAt(numgen.Next(0, rijeci.Count)));
            for (int i = 0; i < rijec.Count(); i++)
            {
                textBox1.Text += "*";
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox2.Text != null)
            {
                if (!rijec.Contains(textBox2.Text)) label1.Text = (Convert.ToInt32(label1.Text) - 1).ToString();
                else
                {
                    pokusaji.Add(textBox2.Text);
                    textBox1.Clear();
                    for (int i = 0; i < rijec.Count(); i++)
                    {
                        if (pokusaji.Contains(rijec.ElementAt(i).ToString()))
                        {
                            textBox1.Text += rijec.ElementAt(i);
                        }
                        else textBox1.Text += "*";
                    }
                }
                textBox2.Text = "";
            }

            if (Convert.ToInt32(label1.Text) == 0)
            {
                MessageBox.Show("Izgubio si"); Application.Exit();
            }

            if (!textBox1.Text.Contains("*"))
            {
                MessageBox.Show("Pobijedio si"); Application.Exit();
            }
        }
    }
}
